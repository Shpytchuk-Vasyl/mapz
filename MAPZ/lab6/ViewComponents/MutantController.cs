using Lab5;
using Mutants;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MutantController : MonoBehaviour
{
    public GameObject[] mutants;
    public static MutantController instance;

    protected void Awake()
    {
        instance = this;
    }

    protected GameObject GetMutant(string name)
    {
        
        return Instantiate(Array.Find(mutants, (mutant) => mutant.name == name));
    }

    public GameObject GetMutant(IMutant mutant)
    {
        GameObject obj = GetMutant(mutant.GetName());
        obj.GetComponent<MutantView>().mutant = mutant.Clone();
        obj.GetComponent<MutantView>().copyMutant = mutant.Clone();
        obj.tag = "Enemy";
        obj.transform.GetChild(0).tag = obj.tag;

        PlayerView view = obj.GetComponent<PlayerView>();
        Destroy(view);

        return obj;
    }

    public GameObject GetPlayer(IMutant mutant)
    {
        GameObject obj = GetMutant(mutant.GetName());
        MutantView mutantView = obj.GetComponent<MutantView>();
        Destroy(mutantView);

        obj.GetComponent<PlayerView>().mutant = mutant.Clone();
        obj.GetComponent<PlayerView>().copyMutant = mutant.Clone();
        obj.tag = "Player";
        obj.name = "Player";
        obj.transform.GetChild(0).tag = obj.tag;


        Debug.Log("Create player");
        return obj;
    }

}
