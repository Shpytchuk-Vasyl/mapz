﻿
class Program
{
    static void Main()
    {
        Lab2_1.DemoFieldsAndFunction.demonstrate();
        Lab2_1.DemoInnerClass.demonstrate();
        Lab2_1.DemoEnum.demonstrate();
        Lab2_1.DemoMultipleInheritance.demonstrate();
        Lab2_1.DemoInitialization.demonstrate();
        Lab2_1.DemoParameter.demonstrate();
        Lab2_1.DemoBoxing.demonstrate();
        Console.WriteLine();

        Lab2_2.MyStruct.demonstrate();
        Lab2_2.DemoInnerStruct.demonstrate();
        Lab2_2.DemoEnum.demonstrate();
        Lab2_2.DemoMultipleInheritance.demonstrate();
        Lab2_2.DemoInitialization.demonstrate();
        Lab2_2.DemoParameter.demonstrate();
        Lab2_2.DemoBoxing.demonstrate();

        Console.WriteLine();
        Lab2.CompareClass.demonstrate();
        Console.WriteLine();
        Lab2.CompareStruct.demonstrate();
        Console.WriteLine();
        Lab2.DemoObject.demonstrate();
        Console.WriteLine();
        Console.ReadLine();
    }





}