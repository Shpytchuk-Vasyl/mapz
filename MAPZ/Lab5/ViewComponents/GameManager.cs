using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using PlayerNamespace;
using Missions;
using System;
using Mutants;
using static Missions.MissionStatistic;
using System.Linq;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public GameData gameData;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        try
        {
            LoadGame();
            Debug.Log("Loaded data:");

        } catch(Exception e)
        {
            Debug.Log("Don't Loaded data:");
            Debug.Log(e.Message);
            firstGame();
        }
       

    }

    private void OnApplicationQuit()
    {
        SaveGame();
    }

    private void SaveGame()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string savePath = Application.persistentDataPath + "/GAME.dat";
        FileStream fileStream = new FileStream(savePath, FileMode.Create);
        formatter.Serialize(fileStream, gameData);
        fileStream.Close();
    }

    private void LoadGame()
    {
        string savePath = Application.persistentDataPath + "/GAME.dat";
        if (File.Exists(savePath))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fileStream = new FileStream(savePath, FileMode.Open);
            gameData = (GameData)formatter.Deserialize(fileStream);
            fileStream.Close();
        } else
        {
            throw new FileNotFoundException();
        }
    }


    private void firstGame()
    {
        gameData.player = new Player
        {
            DNA = 0,
            Experience = 0,
            PlayerName = "New Player",
            Mutant = new Vampire(),
        };
        gameData.missions = new MissionPrototypeFactory().MissionPrototypes.Values.ToArray();
    }



}

[System.Serializable]
public class GameData
{
    public Player player;
    public GameMission[] missions;

}