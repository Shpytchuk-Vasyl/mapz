using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Transform lookAt;
    public float boundX = 1f;
    public float boundY = 1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        Vector3 vector3 = Vector3.zero;
        float deltaX = lookAt.position.x - transform.position.x;
        if(deltaX > boundX || deltaX < - boundX)
        {
            if(transform.position.x < lookAt.position.x) 
            {
                vector3.x = deltaX - boundX;
            } else
            {
                vector3.x = deltaX + boundX;
            }
        }


        float deltaY = lookAt.position.y - transform.position.y;
        if(deltaY > boundY || deltaY < - boundY)
        {
            if (transform.position.y < lookAt.position.y)
            {
                vector3.y = deltaY - boundY;
            }
            else
            {
                vector3.y = deltaY + boundY;
            }
        }

        transform.position += vector3;

    }
}
