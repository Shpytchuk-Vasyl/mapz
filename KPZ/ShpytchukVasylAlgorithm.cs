﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Shpytchuk.Vasyl.RobotChallange;

public class ShpytchukVasylAlgorithm : IRobotAlgorithm
{
    public string Author => "Shpytchuk Vasyl";

    public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
    {
        var robot = robots[robotToMoveIndex];
        if (Round % 10 == 5)
            foreach (var bestPosition in
                     Functions.BestPositions(map, robot.Position, 100))
                if (Functions.IsAvailablePosition(map, robots, bestPosition.Value,
                        Author) && robot.Energy > Functions.DistanceCost(robot.Position, bestPosition.Value))
                    return new MoveCommand
                    {
                        NewPosition = bestPosition.Value
                    };

        if (Functions.GetAuthorRobotCount(robots, Author) < 100 && robot.Energy > 300)
            return new CreateNewRobotCommand();

        IList<EnergyStation> nearbyResources = map.GetNearbyResources(robot.Position,
            2);

        if (nearbyResources.Count > 0)
            foreach (var energyStation in nearbyResources)
                if (Round < 5 || energyStation.Energy > 100)
                    return new CollectEnergyCommand();

        foreach (var bestPosition in
                 Functions.BestPositions(map, robot.Position, 5))
            if (Functions.IsAvailablePosition(map, robots, bestPosition.Value, Author)
                && robot.Energy > Functions.DistanceCost(robot.Position, bestPosition.Value))
                return new MoveCommand
                {
                    NewPosition = bestPosition.Value
                };

        return new CollectEnergyCommand();
    }

    public int Round { get; set; }

    public ShpytchukVasylAlgorithm()
    {
        Logger.OnLogRound += LogRound;
    }

    private void LogRound(object sender, LogRoundEventArgs e)
    {
        Round++;
    }
}

public class Functions
{
    private const int NearbyRadius = 1;
    private const int EnergyRadius = 2;

    public static int DistanceCost(Position center, Position distant)
    {
        return (center.X -
                distant.X) * (center.X - distant.X) +
               (center.Y - distant.Y) *
               (center.Y - distant.Y);
    }

    public static int GetAuthorRobotCount(IList<Robot.Common.Robot> robots, string
        author)
    {
        return ((IEnumerable<Robot.Common.Robot>)robots).Where<Robot.Common.Robot>((Func<Robot.Common.Robot
            , bool>)(robot => robot.OwnerName == author)).Count<Robot.Common.Robot>();
    }

    public static int GetNearbyRobotCount(IList<Robot.Common.Robot> robots, Position
        position)
    {
        return ((IEnumerable<Robot.Common.Robot>)robots).Where<Robot.Common.Robot>((Func<Robot.Common.Robot
            , bool>)(robot => IsWithinRadius(position, robot.Position,
            NearbyRadius))).Count<Robot.Common.Robot>();
    }

    public static bool IsWithinRadius(Position center, Position point, int radius)
    {
        return Math.Abs(center.X - point.X) <= radius && Math.Abs(center.Y - point.Y) <= radius;
    }

    public static bool IsAvailablePosition(
        Map map,
        IList<Robot.Common.Robot> robots,
        Position position,
        string author)
    {
        //if (!map.IsValid(position))
        // return false;
        if (position.X > 100 || position.X < 0 || position.Y > 100 || position.Y < 0)
            return false;
        foreach (var robot in (IEnumerable<Robot.Common.Robot>)robots)
            if (Equals(robot.Position, position) && robot.OwnerName == author)
                return false;

        return true;
    }

    public static List<KeyValuePair<int, Position>> BestPositions(
        Map map,
        Position center,
        int radius)
    {
        var length = 2 * radius + 1;
        var numArray = new int[length, length];
        var num1 = center.Y - radius;
        var num2 = center.X - radius;
        foreach (var station in (IEnumerable<EnergyStation>)map.Stations)
            if (station.Position.X >= num2 - EnergyRadius && station.Position.X < num2 +
                length + EnergyRadius &&
                station.Position.Y >= num1 - EnergyRadius && station.Position.Y < num1 +
                length + EnergyRadius)
            {
                var num3 = station.Position.X - num2;
                var num4 = station.Position.Y - num1;
                for (var index1 = -EnergyRadius; index1 <= EnergyRadius; ++index1)
                for (var index2 = -EnergyRadius; index2 <= EnergyRadius; ++index2)
                    if (num3 + index2 >= 0 && num3 + index2 < length && num4 +
                        index1 >= 0 && num4 + index1 < length)
                        numArray[num4 + index1, num3 + index2] += station.Energy;
            }

        var keyValuePairList = new List<KeyValuePair<int,
            Position>>();
        for (var index3 = 0; index3 < length; ++index3)
        for (var index4 = 0; index4 < length; ++index4)
        {
            numArray[index3, index4] -= DistanceCost(center, new
                Position(num2 + index4, num1 + index3));
            keyValuePairList.Add(new KeyValuePair<int, Position>(numArray[index3,
                index4], new Position(num2 + index4, num1 + index3)));
        }

        keyValuePairList.Sort((Comparison<KeyValuePair<int, Position>>)((pair1, pair2)
            => -pair1.Key.CompareTo(pair2.Key)));
        return keyValuePairList;
    }
}

public sealed class MyEnergyCommand : RobotCommand
{
    public override UpdateViewAfterRobotStepEventArgs ChangeModel(
        IList<Robot.Common.Robot> robots,
        int currentIndex,
        Map map)
    {
        return new UpdateViewAfterRobotStepEventArgs
        {
            TotalEnergyChange = -200
        };
    }
}