using Lab6;
using Mutants;
using System;
using System.Collections;
using UnityEngine;

public class MutantView : Collidable
{

    public IMutant mutant;
    public IMutant copyMutant;



    public float cooldown;
    protected float cooldownRemaining;
    public float pushForce;


    public Animator animator;

    public IPassiveMovement movement = new IdleMovement();
    [SerializeField]
    public MutantMovementContext context = new();



    protected override void Update()
    {
        if (mutant == null) return;

        context.Move();


        animator.SetBool("run", context.isChasing);
        collider.OverlapCollider(contactFilter, hits);
        context.collideWithPlayer = Array.Find(hits, (col) => col != null && col.tag == "Player");
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null) continue;      
            onCollide(hits[i]);
            hits[i] = null;
        }

    }

    protected override void Start()
    { 
        base.Start();
        if (PlaySceneManager.Player != null)     
        context.playerTransform = PlaySceneManager.Player.transform;
        context.transform = transform;
        context.rigidbody = GetComponent<Rigidbody2D>();
        cooldownRemaining = Time.time;
        animator = GetComponent<Animator>(); 
    }

    protected override void onCollide(Collider2D col)
    {
        if (col.tag != "Player") return;
        if(Time.time - cooldownRemaining > cooldown) {
            Damage damage = new Damage
            {
                origin = transform.position,
                pushForce = pushForce,
                sender = mutant
            };
            cooldownRemaining = Time.time;
            animator.SetTrigger("atack");
            col.gameObject.SendMessageUpwards("ReceiveDamge", damage);
        }

    }




    protected void OnDeath()
    {
        animator.SetBool("run", false);
        animator.SetBool("death", true);
        PlaySceneManager.Instance.OnMutantDeath(copyMutant);
        mutant = null;
        StartCoroutine(DestroyAfterDelay(gameObject, 1f));
    }

    protected IEnumerator DestroyAfterDelay(GameObject obj, float delay)
    {
        // Wait for the specified delay
        yield return new WaitForSeconds(delay);

        // Destroy the object
        Destroy(obj);
    }

    public void ReceiveDamge(Damage damage)
    {
        if (mutant != null)
        {
            int minus = damage.sender.Attack(mutant);
            context.pusghDirection = (transform.position - damage.origin).normalized * damage.pushForce;
            cooldownRemaining = Time.time;
            animator.SetTrigger("defend");
            TextMenager.Instance.ShowSimple("-" + minus, Color.red, transform.position);
            if (!mutant.IsAlive()) OnDeath();
        }

    }
}
