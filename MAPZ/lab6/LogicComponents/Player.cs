﻿namespace PlayerNamespace
{
    [System.Serializable]
    public class Player
    {
        public Mutants.IMutant Mutant { get; set; }
        public long Experience { get; set; }
        public long DNA { get; set; }
        public string PlayerName { get; set; }



    };
}
