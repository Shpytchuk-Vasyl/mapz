﻿

using System;

namespace Mutants
{
    public  class Mutant
    {

        public MutantAttribute Speed { get; set; }
        public MutantAttribute Strength { get; set; }
        public String Name { get; set; }
        public MutantAttribute Armor { get; set; }
        public MutantAttribute Health { get; set; }

        public Mutant(string name, MutantAttribute speed, MutantAttribute strength, MutantAttribute armor, MutantAttribute health)
        {
            this.Name = name;
            this.Speed = speed;
            this.Strength = strength;
            this.Armor = armor;
            this.Health = health;
        }

        public class MutantAttribute
        {
            public long Min { get; set; }
            public long Max { get; set; }
            public long Current { get; set; }

            public MutantAttribute(long min, long max)
            {
                this.Min = min;
                this.Max = max;
                GenerateRandomValue();
            }

            public void GenerateRandomValue()
            {
                Current = new Random().Next((int)Min, (int)(Max + 1));
            }


        }

    }

    public class Mission
    {
        public int MaxLevel { get; set; }
        public int CurrentLevel { get; set; }
        public int Reward { get; set; }
        public string Description { get; set; }

        public Mission(int maxLevel, int reward, string description)
        {
            MaxLevel = maxLevel;
            Reward = reward;
            Description = description;
            CurrentLevel = 0;
        }

        public void AddLevel(int level)
        {
            int value = level + CurrentLevel;
            CurrentLevel = (value) >= MaxLevel ? MaxLevel : value;
        }

        public int GetReward()
        {
            if (IsCompleted())
            {
                int value = Reward;
                Reward = 0;
                return value;
            }
            return 0;
        }

        public bool IsCompleted()
        {
            return CurrentLevel >= MaxLevel;
        }

    }


}
