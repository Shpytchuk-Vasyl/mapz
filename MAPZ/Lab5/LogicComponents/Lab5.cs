﻿using Mutants;
using System;
using System.Collections.Generic;

namespace Lab5
{

    public interface IMutantComponent
    {
        string Name { get; }
        void DisplayInfo();
    }

    public class MutantLeaf : IMutantComponent
    {
        public string Name { get; private set; }

        public MutantLeaf(string name)
        {
            Name = name;
        }

        public void DisplayInfo()
        {
            Console.WriteLine($"Mutant: {Name}");
        }
    }

    public class MutantBranch : IMutantComponent
    {
        private readonly List<IMutantComponent> components = new List<IMutantComponent>();
        public string Name { get; private set; }

        public MutantBranch(string name)
        {
            Name = name;
        }

        public void AddComponent(IMutantComponent component)
        {
            components.Add(component);
        }

        public void RemoveComponent(IMutantComponent component)
        {
            components.Remove(component);
        }

        public void DisplayInfo()
        {
            Console.WriteLine($"Mutant Group: {Name}");
            foreach (var component in components)
            {
                component.DisplayInfo();
            }
        }
    }


    public class ComboMutant : IMutant
    {
        private IMutant _mutant;
        private int _combo = 1;

        public ComboMutant(IMutant mutant)
        {
            _mutant = mutant;
        }

        public int Attack(IMutant target)
        {
            int value = 0;
            for (int i = 0; i < _combo; i++)
            {
                value += _mutant.Attack(target);
            }
            _combo++;
            return value;
        }

        public IMutant Clone()
        {
            return new ComboMutant(_mutant.Clone());
        }

        public int Defend(int damage)
        {
            _combo = 1;
            return _mutant.Defend(damage);
        }

        public string GetName()
        {
            return _mutant.GetName();
        }

        public bool IsAlive()
        {
            return _mutant.IsAlive();
        }


        public void Mutate()
        {
            _mutant.Mutate();
        }
    }

    public class InvincibleMutant : IMutant
    {
        private IMutant _mutant;
        private int _invincibleRounds = 3;

        public InvincibleMutant(IMutant mutant)
        {
            _mutant = mutant;
        }

        public InvincibleMutant(IMutant mutant, int invincibleRounds)
        {
            _mutant = mutant;
            _invincibleRounds = invincibleRounds;
        }

        public int Attack(IMutant target)
        {
           return _mutant.Attack(target);
        }

        public IMutant Clone()
        {
            return new InvincibleMutant(_mutant.Clone(), _invincibleRounds);
        }

        public int Defend(int damage)
        {
            if (_invincibleRounds > 0)
            {
                _invincibleRounds--;
            }
            else
            {
              return  _mutant.Defend(damage);
            }
            return 0;
        }

        public string GetName()
        {
            return _mutant.GetName();
        }

        public bool IsAlive()
        {
            return _mutant.IsAlive();
        }


        public void Mutate()
        {
            _mutant.Mutate();
        }
    }

}
