using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingText
{
    public bool active;
    public GameObject go;
    public Text text;
    public Vector3 motion;
    public float duration;
    public float lastShown;

    public FloatingText()
    {
    }

    public FloatingText(bool active, GameObject go, Text text, Vector3 motion, float duration)
    {
        this.active = active;
        this.go = go;
        this.text = text;
        this.motion = motion;
        this.duration = duration;
        this.lastShown = Time.time;
    }




    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public void Update()
    {
        if (!active) return;
        if (Time.time - lastShown > duration) Hide();

        go.transform.position += motion * Time.deltaTime;
    }


    public void Show()
    {
        active = true;
        lastShown = Time.time;
        go.SetActive(active);
    }

    public void Hide()
    {
        active = false;
        go.SetActive(false);
    }

}
