using Lab4;
using Mutants;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Spawner : Collidable
{
    public string[] MutantTypes;
    public int MaxCount = 5;
    public float Interval = 10f;
    private float LastSpawn;
    public int CurrentCount = 0;
    protected IMutantSpawner spawner = new MutantSpawner();

    protected override void Start()
    {
        collider = transform.GetComponent<BoxCollider2D>();
        hits = new Collider2D[500];
        /*spawner.AddRange(Array.ConvertAll(MutantTypes, 
            t => (IGenerator)Activator
            .CreateInstance(typeof(AbstractGenerator<>)
            .MakeGenericType(Type.GetType("Mutants." + t))))
            .Where(g => g != null)
            .ToArray());*/
        spawner.AddRange(Array.ConvertAll(MutantTypes,
            t => "Mutants." + t));
    }

    protected override void Update()
    {
        CurrentCount = 0;
        base.Update();
        
        if (CurrentCount < MaxCount && Time.time - LastSpawn > Interval)
        {
            GameObject mutant = MutantController.instance.GetMutant(spawner.getNext());
            mutant.transform.position = transform.position;
            mutant.GetComponent<MutantView>().context.playerTransform = PlaySceneManager.Player.transform;
            LastSpawn = Time.time;
        } 

    }
    protected override void onCollide(Collider2D col)
    {
        if(col.tag == "Enemy")
            CurrentCount++;
    }
}
