﻿using Mutants;
using System;

namespace Lab4
{
    public interface Generator
    {
        public Mutant Generate();
    }


    public abstract class AbstractGenerator<T> : Generator where T : Mutant, new()
    {
        private Random r = new Random();
        public Mutant Generate()
        {
            return (Mutant)Activator.CreateInstance(typeof(T), r);
        }
    }



    public class SkeletonGenerator : AbstractGenerator<Skeleton>
    {
    }

    public class GoblinGenerator : AbstractGenerator<Goblin>
    {

    }
    public class OrcGenerator : AbstractGenerator<Orc>
    {
    }
    
    public class TrollGenerator : AbstractGenerator<Troll>
    {
    }

    public class VampireGenerator : AbstractGenerator<Vampire>
    {
    }

    public class WerewolfGenerator : AbstractGenerator<Werewolf>
    { }

    public class GhostGenerator : AbstractGenerator<Ghost>
    { }


    class MutantGenerator
    {
       public Generator[] Generators { get; set; }
       private Random random = new Random();

        public MutantGenerator(Generator[] generators)
        {
            this.Generators = generators;
        }

        public MutantGenerator()
        {
        }

        public Mutant getNext()
        {
            return Generators[random.Next(Generators.Length)].Generate();
        } 

    }




}
