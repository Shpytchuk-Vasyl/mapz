﻿using System;
using System.Collections.Generic;

namespace Mutants
{

    public interface Prototype<T>
    {
       T Clone();
    }


   
    public interface IMutant : Prototype<IMutant>
    {
        int Attack(IMutant target);

        int Defend(int damage);

        void Mutate();
        
        bool IsAlive();

        String GetName();

    }


    [System.Serializable]
    public class MutantAttribute : Prototype<MutantAttribute>
    {
        public int Min { get; set; }
        public int Max { get; set; }
        public int Current { get; set; }

        public MutantAttribute(int min, int max, Random random)
        {
            this.Min = min;
            this.Max = max;
            GenerateRandomCurrentValue(random);
        }

        public MutantAttribute(int min, int max, int current)
        {
            this.Min = min;
            this.Max = max;
            this.Current = current;
        }

        public void GenerateRandomCurrentValue(Random random)
        {
            Current = random.Next(Min, (Max + 1));
        }

        public  MutantAttribute Clone()
        {
            return new MutantAttribute(Min, Max, Current);
        }
    }



    [System.Serializable]
    public class Mutant : IMutant
    {

        public Mutant(string name, MutantAttribute speed, MutantAttribute strength, MutantAttribute armor, MutantAttribute health)
        {
            this.Name = name;
            this.Speed = speed;
            this.Strength = strength;
            this.Armor = armor;
            this.Health = health;
        }

        public Mutant(Random r) : this("Mutant", new MutantAttribute(1, 500, r), new MutantAttribute(1, 500, r), new MutantAttribute(1, 100, r), new MutantAttribute(1, 1000, r)) { }

        public Mutant() : this(new Random()) { }



        public MutantAttribute Speed { get; set; }
        public MutantAttribute Strength { get; set; }
        public String Name { get; set; }
        public MutantAttribute Armor { get; set; }
        public MutantAttribute Health { get; set; }

        public int Attack(IMutant target)
        {
            return target.Defend(Strength.Current);
        }

        public int Defend(int damage)
        {
            int value = damage - Armor.Current;
            if(value > 0)
            {
                Health.Current -= value;
            }
            return value;
        }

        public void Mutate()
        {
            Random random = new Random();

            switch (random.Next(1, 5))
            {
                case 0: Speed.GenerateRandomCurrentValue(random); break;
                case 1: Health.GenerateRandomCurrentValue(random); break;
                case 3: Strength.GenerateRandomCurrentValue(random); break;
                case 4: Armor.GenerateRandomCurrentValue(random); break;
                default: break;
            }

        }

        public IMutant Clone()
        {
            return new Mutant((string)this.Name.Clone(), this.Speed.Clone(), this.Strength.Clone(), this.Armor.Clone(), this.Health.Clone());
        }

        public bool IsAlive()
        {
            return Health.Current > 0;
        }

        public void Defend(long damage)
        {
            throw new NotImplementedException();
        }

        public string GetName()
        {
            return Name;
        }
    }


    public class MutantPrototypeFactory
    {
        private readonly Dictionary<string, Mutant> prototypes;
        Random r = new Random();
        public MutantPrototypeFactory()
        {
            

            Mutant Goblin = new Goblin(r);
            Mutant Dragon = new Dragon(r);
            Mutant Skeleton = new Skeleton(r);
            Mutant Orc = new Orc(r);
            Mutant Vampire = new Vampire(r);
            Mutant Troll = new Troll(r);
            Mutant Werewolf = new Werewolf(r);
            Mutant Ghost = new Ghost(r);

            this.prototypes = new Dictionary<string, Mutant>
                {
                    {Goblin.Name, Goblin },
                    {Dragon.Name, Dragon },
                    {Skeleton.Name, Skeleton },
                    {Orc.Name, Orc },
                    {Vampire.Name, Vampire },
                    {Troll.Name, Troll },
                    {Werewolf.Name, Werewolf },
                    { Ghost.Name, Ghost },
                }; 
        }

        public void AddPrototype(String key, Mutant prototype)
        {
            this.prototypes[key] = prototype;
        }

        public Mutant CreateRandomMutant()
        {
            
            List<string> keys = new List<string>(this.prototypes.Keys);
            return CreateMutant(keys[r.Next(keys.Count)]);
        }

        public Mutant CreateMutant(String key)
        {
            if (this.prototypes.ContainsKey(key))
            {
                Mutant mutant = this.prototypes[key];
                
                (r.Next(4) switch
                {
                    0 => mutant.Speed,
                    1 => mutant.Strength,
                    2 => mutant.Armor,
                    _ => mutant.Health
                }).GenerateRandomCurrentValue(r);

                return (Mutant)mutant.Clone();
            }
            else
            {
                throw new KeyNotFoundException($"Prototype with key '{key}' not found.");
            }
        }
    }
    [System.Serializable]

    public class Goblin : Mutant
    {
        public Goblin(Random r) : base("Goblin", new MutantAttribute(1, 100, r), new MutantAttribute(1, 100, r), new MutantAttribute(1, 100, r), new MutantAttribute(1, 1000, r)) { }
        public Goblin() : this(new Random()) { }


    }
    [System.Serializable]

    public class Dragon : Mutant
    {
        public Dragon(Random r) : base("Dragon", new MutantAttribute(1, 200, r), new MutantAttribute(1, 200, r), new MutantAttribute(1, 150, r), new MutantAttribute(1, 2000, r)) { }
        public Dragon() : this(new Random()) { }

    }
    [System.Serializable]

    public class Skeleton : Mutant
    {
        public Skeleton(Random r) : base("Skeleton", new MutantAttribute(1, 50, r), new MutantAttribute(1, 50, r), new MutantAttribute(1, 50, r), new MutantAttribute(1, 500, r)) { }
        public Skeleton() : this(new Random()) { }

    }
    [System.Serializable]

    public class Orc : Mutant
    {
        public Orc(Random r) : base("Orc", new MutantAttribute(1, 150, r), new MutantAttribute(1, 150, r), new MutantAttribute(1, 100, r), new MutantAttribute(1, 1500, r)) { }
        public Orc() : this(new Random()) { }

    }
    [System.Serializable]

    public class Vampire : Mutant
    {
        public Vampire(Random r) : base("Vampire", new MutantAttribute(1, 120, r), new MutantAttribute(1, 120, r), new MutantAttribute(1, 80, r), new MutantAttribute(1, 800, r)) { }
        public Vampire() : this(new Random()) { }

    }
    [System.Serializable]

    public class Troll : Mutant
    {
        public Troll(Random r) : base("Troll", new MutantAttribute(1, 180, r), new MutantAttribute(1, 180, r), new MutantAttribute(1, 120, r), new MutantAttribute(1, 1800, r)) { }
        public Troll() : this(new Random()) { }

    }
    [System.Serializable]

    public class Werewolf : Mutant
    {
        public Werewolf(Random r) : base("Werewolf", new MutantAttribute(1, 130, r), new MutantAttribute(1, 130, r), new MutantAttribute(1, 90, r), new MutantAttribute(1, 1300, r)) { }
        public Werewolf() : this(new Random()) { }
    }
    [System.Serializable]

    public class Ghost : Mutant
    {
        public Ghost(Random r) : base("Ghost", new MutantAttribute(1, 80, r), new MutantAttribute(1, 80, r), new MutantAttribute(1, 60, r), new MutantAttribute(1, 400, r)) { }
        public Ghost() : this(new Random()) { }

    }

    




}
