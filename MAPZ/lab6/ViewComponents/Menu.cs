using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
   public void OnPlay()
    {
        SceneManager.LoadScene("Play");
    }

    public void OnPlayer()
    {
        SceneManager.LoadScene("Player");
    }
    public void OnMission()
    {
        SceneManager.LoadScene("Mission");
    }
    public void OnExit()
    {
        Application.Quit();
    }

    public void OnMenu()
    {
        SceneManager.LoadScene("Menu");
    }

}
