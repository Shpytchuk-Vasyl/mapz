﻿using Mutants;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Missions
{

    public interface IMission
    {
        public void IncreaseProgress(int level);

        public IReward? GetReward();

        public bool IsCompleted();

        public bool IsLocked();
    }

    public interface IReward
    {
        public void RewardUser();
        public string GetDescription();
    }


    [System.Serializable]
    public abstract class AbstractMission : IMission
    {
        public int MaxProgress { get; set; }
        public int CurrentProgress { get; set; }
        public IReward? Reward { get; set; }
        public string Description { set; get; }

        public AbstractMission(int maxProgress, IReward reward, string description)
        {
            MaxProgress = maxProgress;
            CurrentProgress = 0;
            Reward = reward;
            Description = description;
        }

        public IReward? GetReward()
        {
            if (IsCompleted())
            {
                IReward value = Reward;
                Reward = null;
                return value;
            }
            return null;
        }

        public bool IsCompleted() => CurrentProgress >= MaxProgress;

        public void IncreaseProgress(int level)
        {
            int value = level + CurrentProgress;
            CurrentProgress = (value) >= MaxProgress ? MaxProgress : value;
        }

        public string GetDescription() => Description;
        public abstract bool IsLocked();

    }

    [System.Serializable]

    public class SimpleMission : AbstractMission
    {
        public SimpleMission(int maxProgress, IReward reward, string description) : base(maxProgress, reward, description) { }

        public override bool IsLocked() => false;

        public class MissionBuilder
        {
            private int MaxProgress { get; set; }
            private String Description { get; set; }
            private IReward Reward { get; set; }

            public MissionBuilder WithMaxProgress(int max)
            {
                MaxProgress = max;
                return this;
            }

            public MissionBuilder WithDescription(String description)
            {
                Description = description;
                return this;
            }

            public MissionBuilder WithReward(IReward reward)
            {
                Reward = reward;
                return this;
            }

            public SimpleMission Build()
            {
                return new SimpleMission(MaxProgress, Reward, Description);
            }
        }

    }

    [System.Serializable]
    public delegate void Calculate(GameMission mission, MissionStatistic gameStatistic);
 
    [System.Serializable]
    public class GameMission : AbstractMission
    {
        public GameMission(int maxProgress, IReward reward, string description, Calculate calculateDelegate, string Id)
            : base(maxProgress, reward, description)
        {
            CalculateDelegate = calculateDelegate;
            ID = ID;
        }

        public string ID { get; set; }
        public bool Locked { get; set; }

        public override bool IsLocked() => Locked;

        public Calculate CalculateDelegate { get; set; }

        public void AfteGame(MissionStatistic gameStatistic)
        {
            CalculateDelegate(this, gameStatistic);
        }

    }


    [System.Serializable]
    public class MissionStatistic
    {
        public int AllDamage { get; set; }
        public int AllMutants { get; set; }
        public Dictionary<string, int> MutantCount;
        //other field

        public MissionStatistic()
        {
            MutantCount = new Dictionary<string, int>();
        }

        public void AddMutant(IMutant mutant)
        {
            AllDamage += ((Mutant)mutant).Health.Current;
            AllMutants += 1;
            if (MutantCount.ContainsKey(mutant.GetName()))
            {
                MutantCount[mutant.GetName()]++;
            }
            else
            {
                MutantCount.Add(mutant.GetName(), 1);
            }
        }


        [System.Serializable]
        public class MissionPrototypeFactory
        {
          
            public Dictionary<string, GameMission> MissionPrototypes { get; private set; }

            public MissionPrototypeFactory()
            {
                MissionPrototypes = new Dictionary<string, GameMission>
                {
            { "Mission1", new GameMission(10, new Reward(), "Kill 10 dragons ", Kill10Dragon, "Mission1") },
            { "Mission2", new GameMission(5, new Reward(), "Kill 5 mutants", Kill5Mutant, "Mission2") },
            { "Mission3", new GameMission(15, new Reward(), "Deal 10000 damage", Deal10000Damage, "Mission3") },
                };
            }


            [System.Serializable]
            class Reward : IReward
            {
                public string GetDescription()
                {
                    return "Not implemented yet";
                }

                public void RewardUser()
                {
                    
                }
            }


            private void Kill10Dragon(GameMission mission, MissionStatistic gameStatistic)
            {
                if (gameStatistic.MutantCount.ContainsKey("Dragon"))
                    mission.IncreaseProgress(gameStatistic.MutantCount["Dragon"]);
            }

            private void Kill5Mutant(GameMission mission, MissionStatistic gameStatistic)
            {
                mission.IncreaseProgress(gameStatistic.AllMutants);
            }

            private void Deal10000Damage(GameMission mission, MissionStatistic gameStatistic)
            {
                mission.IncreaseProgress(gameStatistic.AllDamage);
            }

        }






    }


}
