using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collidable : MonoBehaviour
{
    public ContactFilter2D contactFilter;
    protected Collider2D[] hits;
    protected BoxCollider2D collider;
    protected virtual void Start()
    {
        try
        {
            collider = transform.GetChild(0).GetComponent<BoxCollider2D>();
        } catch { }
        hits = new Collider2D[50];

        
    }

    protected virtual void Update()
    {
        
        collider.OverlapCollider(contactFilter, hits);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null) continue;
            onCollide(hits[i]);
            hits[i] = null;
        }
    }

    protected virtual void onCollide(Collider2D col)
    {
       
    }

}
