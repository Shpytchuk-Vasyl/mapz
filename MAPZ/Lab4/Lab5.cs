﻿using Missions;
using Mutants;
using PlayerNamespace;
using System;


namespace Lab5
{
    class Game
    {
        private IMission[] Missions;
        private Player player;


        public Game() {
            Missions = Array.Empty<IMission>();//dowload from file in feature
            player = Player.Instance;
            //other propertis 
        }



        public void Play()
        {
            GameMap gameMap = new();
            gameMap.Start();
        }






    }











    public class ComboMutant : IMutant
    {
        private IMutant _mutant;
        private int _combo = 1;

        public ComboMutant(IMutant mutant)
        {
            _mutant = mutant;
        }

        public void Attack(IMutant target)
        {
            for (int i = 0; i < _combo; i++)
            {
                _mutant.Attack(target);
            }
            _combo++;
        }

        public IMutant Clone()
        {
            return new ComboMutant(_mutant.Clone());
        }

        public void Defend(long damage)
        {
            _combo = 1;
            _mutant.Defend(damage);
        }

        public void Move()
        {
            _mutant.Move();
        }

        public void Mutate()
        {
            _mutant.Mutate();
        }
    }

    public class InvincibleMutant : IMutant
    {
        private IMutant _mutant;
        private int _invincibleRounds = 3;

        public InvincibleMutant(IMutant mutant)
        {
            _mutant = mutant;
        }

        public InvincibleMutant(IMutant mutant, int invincibleRounds)
        {
            _mutant = mutant;
            _invincibleRounds = invincibleRounds;
        }

        public void Attack(IMutant target)
        {
            _mutant.Attack(target);
        }

        public IMutant Clone()
        {
            return new InvincibleMutant(_mutant.Clone(), _invincibleRounds);
        }

        public void Defend(long damage)
        {
            if (_invincibleRounds > 0)
            {
                _invincibleRounds--;
            }
            else
            {
                _mutant.Defend(damage);
            }
        }

        public void Move()
        {
            _mutant.Move();
        }

        public void Mutate()
        {
            _mutant.Mutate();
        }
    }


    class GameMap
    {
        

        public GameMap() {
            // Generate random map with random mutants
        }

        public void Start()
        {

        }

        


        //TODO in feature
    }




}
