using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextMenager : MonoBehaviour
{
    public GameObject textContainer;
    public GameObject texPrefab;
    private List<FloatingText> textList = new List<FloatingText>(10);
    public static TextMenager Instance;


    void Start()
    {
        Instance = this;
    }

    void Update()
    {
        foreach (FloatingText text in textList)
        {
            text.Update();
        }
    }



    private FloatingText GetFloatingText()
    {
        FloatingText text = textList.Find(t => !t.active);
        if (text == null)
        {
            text = new FloatingText();
            text.go = Instantiate(texPrefab); ;
            text.go.transform.SetParent(textContainer.transform);
            text.text = text.go.GetComponent<Text>();
            textList.Add(text);
        }
        return text;
    }

    public void Show(string msg, int fontSize, Color color, Vector3 position, Vector3 motion, float duration)
    {
        FloatingText floatingText = GetFloatingText();
        floatingText.text.text = msg;
        floatingText.text.fontSize = fontSize;
        floatingText.text.color = color;
        floatingText.motion = motion;
        floatingText.duration = duration;
        floatingText.go.transform.position = Camera.main.WorldToScreenPoint(position);
        floatingText.Show();
    }

    public void ShowSimple(string msg, Color color, Vector3 position)
    {
        Show(msg, 22, color, position, Vector3.up * 10, 3);
    }

}
