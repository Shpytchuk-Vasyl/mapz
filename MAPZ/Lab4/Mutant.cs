﻿

using Lab4;
using System;

namespace Mutants
{

    public interface Prototype<T>
    {
        T Clone();
    }


    public interface IMutant : Prototype<IMutant>
    {
        void Attack(IMutant target);

        void Defend(long damage);

        void Mutate();

        void Move();
    }

    

    public class MutantAttribute : Prototype<MutantAttribute>
    {
        public long Min { get; set; }
        public long Max { get; set; }
        public long Current { get; set; }

        public MutantAttribute(long min, long max, Random random)
        {
            this.Min = min;
            this.Max = max;
            GenerateRandomCurrentValue(random);
        }

        public MutantAttribute(long min, long max, long current)
        {
            this.Min = min;
            this.Max = max;
            this.Current = current;
        }

        public void GenerateRandomCurrentValue(Random random)
        {
            Current = random.Next((int)Min, (int)(Max + 1));
        }

        public  MutantAttribute Clone()
        {
            return new MutantAttribute(Min, Max, Current);
        }
    }




    public class Mutant : IMutant
    {

        public Mutant(string name, MutantAttribute speed, MutantAttribute strength, MutantAttribute armor, MutantAttribute health)
        {
            this.Name = name;
            this.Speed = speed;
            this.Strength = strength;
            this.Armor = armor;
            this.Health = health;
        }

        public Mutant(Random r) : this("Mutant", new MutantAttribute(1, 500, r), new MutantAttribute(1, 500, r), new MutantAttribute(1, 100, r), new MutantAttribute(1, 1000, r)) { }

        public Mutant() : this(new Random()) { }



        public MutantAttribute Speed { get; set; }
        public MutantAttribute Strength { get; set; }
        public String Name { get; set; }
        public MutantAttribute Armor { get; set; }
        public MutantAttribute Health { get; set; }

        public void Attack(IMutant target)
        {
            throw new NotImplementedException();
        }

        public void Defend(long damage)
        {
            throw new NotImplementedException();
        }

        public void Mutate()
        {
            throw new NotImplementedException();
        }

        public void Move()
        {
            throw new NotImplementedException();
        }

        public IMutant Clone()
        {
            return new Mutant(this.Name, this.Speed.Clone(), this.Strength.Clone(), this.Armor.Clone(), this.Health.Clone());
        }
    }


    public class MutantPrototypeFactory
    {
        private readonly Dictionary<string, Mutant> prototypes;

        public MutantPrototypeFactory()
        {
            Random r = new Random();

            Mutant Goblin = new Goblin(r);
            Mutant Dragon = new Dragon(r);
            Mutant Skeleton = new Skeleton(r);
            Mutant Orc = new Orc(r);
            Mutant Vampire = new Vampire(r);
            Mutant Troll = new Troll(r);
            Mutant Werewolf = new Werewolf(r);
            Mutant Ghost = new Ghost(r);

            this.prototypes = new Dictionary<string, Mutant>
                {
                    {Goblin.Name, Goblin },
                    {Dragon.Name, Dragon },
                    {Skeleton.Name, Skeleton },
                    {Orc.Name, Orc },
                    {Vampire.Name, Vampire },
                    {Troll.Name, Troll },
                    {Werewolf.Name, Werewolf },
                    { Ghost.Name, Ghost },
                }; 
        }

        public void AddPrototype(String key, Mutant prototype)
        {
            this.prototypes[key] = prototype;
        }

        public Mutant CreateRandomMutant()
        {
            Random random = new Random();
            List<string> keys = new List<string>(this.prototypes.Keys);
            return CreateMutant(keys[random.Next(keys.Count)]);
        }

        public Mutant CreateMutant(String key)
        {
            if (this.prototypes.ContainsKey(key))
            {
                Mutant mutant = this.prototypes[key];
                Random rnd = new Random();
                (rnd.Next(4) switch
                {
                    0 => mutant.Speed,
                    1 => mutant.Strength,
                    2 => mutant.Armor,
                    _ => mutant.Health
                }).GenerateRandomCurrentValue(rnd);

                return (Mutant)mutant.Clone();
            }
            else
            {
                throw new KeyNotFoundException($"Prototype with key '{key}' not found.");
            }
        }
    }

    public class Goblin : Mutant
    {
        public Goblin(Random r) : base("Goblin", new MutantAttribute(1, 100, r), new MutantAttribute(1, 100, r), new MutantAttribute(1, 100, r), new MutantAttribute(1, 1000, r)) { }
        public Goblin() : this(new Random()) { }


    }

    public class Dragon : Mutant
    {
        public Dragon(Random r) : base("Dragon", new MutantAttribute(1, 200, r), new MutantAttribute(1, 200, r), new MutantAttribute(1, 150, r), new MutantAttribute(1, 2000, r)) { }
        public Dragon() : this(new Random()) { }

    }

    public class Skeleton : Mutant
    {
        public Skeleton(Random r) : base("Skeleton", new MutantAttribute(1, 50, r), new MutantAttribute(1, 50, r), new MutantAttribute(1, 50, r), new MutantAttribute(1, 500, r)) { }
        public Skeleton() : this(new Random()) { }

    }

    public class Orc : Mutant
    {
        public Orc(Random r) : base("Orc", new MutantAttribute(1, 150, r), new MutantAttribute(1, 150, r), new MutantAttribute(1, 100, r), new MutantAttribute(1, 1500, r)) { }
        public Orc() : this(new Random()) { }

    }

    public class Vampire : Mutant
    {
        public Vampire(Random r) : base("Vampire", new MutantAttribute(1, 120, r), new MutantAttribute(1, 120, r), new MutantAttribute(1, 80, r), new MutantAttribute(1, 800, r)) { }
        public Vampire() : this(new Random()) { }

    }

    public class Troll : Mutant
    {
        public Troll(Random r) : base("Troll", new MutantAttribute(1, 180, r), new MutantAttribute(1, 180, r), new MutantAttribute(1, 120, r), new MutantAttribute(1, 1800, r)) { }
        public Troll() : this(new Random()) { }

    }

    public class Werewolf : Mutant
    {
        public Werewolf(Random r) : base("Werewolf", new MutantAttribute(1, 130, r), new MutantAttribute(1, 130, r), new MutantAttribute(1, 90, r), new MutantAttribute(1, 1300, r)) { }
        public Werewolf() : this(new Random()) { }
    }

    public class Ghost : Mutant
    {
        public Ghost(Random r) : base("Ghost", new MutantAttribute(1, 80, r), new MutantAttribute(1, 80, r), new MutantAttribute(1, 60, r), new MutantAttribute(1, 400, r)) { }
        public Ghost() : this(new Random()) { }

    }

    




}
