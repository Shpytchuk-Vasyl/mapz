using Cainos.PixelArtTopDown_Basic;
using Missions;
using Mutants;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlaySceneManager : MonoBehaviour
{
    public MissionStatistic statistic;
    public static PlaySceneManager Instance;
    public static GameObject Player;


    private void Start()
    {
        statistic = new MissionStatistic();
        Instance = this;
        Debug.Log(GameManager.instance);
        Debug.Log(GameManager.instance.gameData);
        Debug.Log(GameManager.instance.gameData.player);
        Debug.Log(GameManager.instance.gameData.player.Mutant);
        Debug.Log("clone mutant");
        Debug.Log(MutantController.instance);



        Player = MutantController.instance
            .GetPlayer(GameManager.instance.gameData.player.Mutant.Clone());
        Player.transform.position = Vector3.zero;

        Camera.main.GetComponent<CameraFollow>().target = Player.transform;

    }


    public void OnMutantDeath(IMutant mutant)
    {
        statistic.AddMutant(mutant);
    }


    public void OnPlayerDeath()
    {
        Array.ForEach(GameManager.instance.gameData.missions, (mission) =>
                            mission.AfteGame(statistic)
        );
        GameManager.instance.gameData.player.DNA += statistic.AllMutants * 2;
        SceneManager.LoadScene("Menu");
    }



}
