﻿using Mutants;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UIElements.Experimental;

namespace Lab5
{
    public class PlayerView : MutantView
    {
        

        protected override void Start()
        {
            base.Start();   
        }


        protected override void Update()
        {
            if (mutant == null) return;
            base.Update();
            float x = Input.GetAxisRaw("Horizontal");
            float y = Input.GetAxisRaw("Vertical");
            if (x > 0) transform.localScale = new Vector3(math.abs(transform.localScale.x), transform.localScale.y, 1);
            if (x < 0) transform.localScale = new Vector3(-math.abs(transform.localScale.x), transform.localScale.y, 1);

            Vector2 dir = new Vector2(x, y);
            dir.Normalize();

            animator.SetBool("run", dir != Vector2.zero);
           

           context.rigidbody.velocity = context.speed * dir;
        }

        protected override void onCollide(Collider2D col)
        {
            
            if (Time.time - cooldownRemaining > cooldown)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    Damage damage = new Damage
                    {
                        origin = transform.position,
                        pushForce = pushForce,
                        sender = mutant
                    };
                    animator.SetTrigger("atack");
                    if (col.tag != "Enemy") return;
                    col.gameObject.SendMessageUpwards("ReceiveDamge", damage);
                }
            }
        }

        public new void ReceiveDamge(Damage damage)
        {
            int minus = damage.sender.Attack(mutant);
            TextMenager.Instance.ShowSimple("-" + minus, Color.red, transform.position);
            animator.SetTrigger("defend");
            Debug.Log(((Mutant)mutant).Health.Current);
            if (!mutant.IsAlive()) OnDeath();
        }

        protected new void OnDeath()
        {
            animator.SetBool("run", false);
            animator.SetBool("death", true);
            mutant = null;
            StartCoroutine(DestroyAfterDelay(gameObject, 1f));
            PlaySceneManager.Instance.OnPlayerDeath();
        }

    }
}
