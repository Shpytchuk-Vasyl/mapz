﻿using System;

namespace Missions
{

    public interface IMission
    {
        public void IncreaseProgress(int level);

        public IReward? GetReward();

        public bool IsCompleted();

        public bool IsLocked();
    }

    public interface IReward
    {
        public void RewardUser();
    } 
    

    public abstract class AbstractMission : IMission
    {
        public int MaxProgress { get; set; }
        public int CurrentProgress { get; set; }
        public IReward? Reward { private get;  set; }
        private String Description { set; get; }

        public AbstractMission(int maxProgress, IReward reward, String description)
        {
            MaxProgress = maxProgress;
            CurrentProgress = 0;
            Reward = reward;
            Description = description;
        }

        public IReward? GetReward()
        {
            if (IsCompleted())
            {
                IReward value = Reward;
                Reward = null;
                return value;
            }
            return null;
        }

        public bool IsCompleted() => CurrentProgress >= MaxProgress;

        public void IncreaseProgress(int level)
        {
            int value = level + CurrentProgress;
            CurrentProgress = (value) >= MaxProgress ? MaxProgress : value;
        }

        public String GetDescription() => Description;
        public abstract bool IsLocked();

    }

    public class SimpleMission : AbstractMission
    {
        public SimpleMission(int maxProgress, IReward reward, string description) : base(maxProgress, reward, description){}

        public override bool IsLocked() => false;

        public class MissionBuilder
        {
            private int MaxProgress { get; set; }
            private String Description { get; set; }
            private IReward Reward { get; set; }

            public MissionBuilder WithMaxProgress(int max)
            {
                MaxProgress = max;
                return this;
            }

            public MissionBuilder WithDescription(String description)
            {
                Description = description;
                return this;
            }

            public MissionBuilder WithReward(IReward reward)
            {
                Reward = reward;
                return this;
            }

            public SimpleMission Build()
            {
                return new SimpleMission(MaxProgress, Reward, Description);
            }
        }

    }

}
