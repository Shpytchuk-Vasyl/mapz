﻿using Mutants;
using System;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.CompositeCollider2D;

namespace Lab4
{
    public interface IGenerator
    {
        public IMutant Generate();
    }

    public interface IMutantSpawner
    {
        void Add(IGenerator generator);
        void Add(Type type);
        void Add(string type);

        void AddRange(params IGenerator[] generators);
        void AddRange(params Type[] types);
        void AddRange(params string[] types);
        List<IGenerator> GetAll();
        void RemoveAll();
        IGenerator removeAll(string monsterName);
        IGenerator removeFirst(string monsterName);
        IMutant getNext();
    }

    public class AbstractGenerator<T> : IGenerator where T : IMutant, new()
    {
        private System.Random r = new System.Random();
        public IMutant Generate()
        {
            return (IMutant)Activator.CreateInstance(typeof(T), r);
        }

        public AbstractGenerator() { }
    }

    class MutantSpawner : IMutantSpawner
    {
        public List<IGenerator> Generators { get; set; }
        private System.Random random;

        public MutantSpawner()
        {
            Generators = new List<IGenerator>();
            random = new System.Random();
        }

        public MutantSpawner(IGenerator[] generators) =>
            Generators = new List<IGenerator>(generators);


        public MutantSpawner(params Type[] generators) =>
            Generators = new List<IGenerator>(Array.ConvertAll(generators, t => (IGenerator)Activator.CreateInstance(t)));

        public void Add(Type type) =>
            Generators.Add((IGenerator)Activator.CreateInstance(typeof(AbstractGenerator<>).MakeGenericType(type)));

        public void Add(IGenerator generator) =>
            Generators.Add(generator);

        public void AddRange(params IGenerator[] generators)
            => Generators.AddRange(generators);

        public void AddRange(params Type[] types) =>
            Generators.AddRange(Array.ConvertAll(types, t => (IGenerator)Activator.CreateInstance(typeof(AbstractGenerator<>).MakeGenericType(t))));

        public List<IGenerator> GetAll() => Generators;

        public IGenerator removeFirst(String monsterName)
        {
            IGenerator generatorToRemove = Generators.Find(generator => generator.Generate().GetName() == monsterName);
            if (generatorToRemove != null)
            {
                Generators.Remove(generatorToRemove);
            }
            return generatorToRemove;
        }

        public IGenerator removeAll(String monsterName)
        {
            IGenerator generatorToRemove = Generators.Find(generator => generator.Generate().GetName() == monsterName);
            if (generatorToRemove != null)
            {
                Generators.Remove(generatorToRemove);
            }
            return generatorToRemove;
        }

        public void RemoveAll() =>
            Generators.Clear();


        public IMutant getNext() => 
             Generators[random.Next(Generators.Count)].Generate();
        public void Add(string mutant) =>
            Generators.Add((IGenerator)Activator.CreateInstance(typeof(AbstractGenerator<>).MakeGenericType(Type.GetType(mutant))));

        public void AddRange(params string[] types) =>
            Generators.AddRange(Array.ConvertAll(types, t => (IGenerator)Activator.CreateInstance(typeof(AbstractGenerator<>).MakeGenericType(Type.GetType(t)))));
        
    }


}
