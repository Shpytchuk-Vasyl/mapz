﻿using Mutants;
using System;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using static UnityEditor.Timeline.TimelinePlaybackControls;

namespace Lab6
{

    /// <summary>
    ///  Strategy pattern
    /// </summary>
    public interface IPassiveMovement
    {
        void Move(Rigidbody2D rb);
    }

    public class CircleMovement : IPassiveMovement
    {
        public float radius = 4f;
        public float speed = 2f;

        public CircleMovement(float speed)
        {
            this.speed = speed;
        }

        public void Move(Rigidbody2D rb)
        {
            float angle = Time.time * speed;
            Vector2 move = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * radius;

            rb.velocity = move.normalized;
        }
    }

    public class HorizontalMovement : IPassiveMovement
    {
        public float speed = 2f;
        private bool moveRight = true;
        public int currentCountMove = 0, countMove = 5;

        public HorizontalMovement(float speed, int countMove)
        {
            this.speed = speed;
            this.countMove = countMove;
        }

        public void Move(Rigidbody2D rb)
        {
            if (currentCountMove >= countMove)
            {
                moveRight = !moveRight;
                currentCountMove = 0;
            }

            Vector2 moveDirection = (moveRight ? Vector2.right : -Vector2.right) * speed;
            rb.velocity = moveDirection.normalized ;

            currentCountMove++;
        }
    }

    public class IdleMovement : IPassiveMovement
    {
        public void Move(Rigidbody2D rb)
        {
            rb.velocity = Vector2.zero;
        }
    }





    /// <summary>
    ///  Stage pattern
    /// </summary>


    [Serializable]
    public class MutantMovementContext
    {
        public  Rigidbody2D rigidbody;
        public bool isChasing;
        public  Transform playerTransform, transform;
        public Vector3 pusghDirection;

        public float pushRecovery;
        public float speed;
        public float triggerLength;
        public float chaseLength;
        public bool  collideWithPlayer;

        public IStage stage;

        public MutantMovementContext() {
            stage = new NoneStage(this);
        }

        public void Move()
        {
            stage.Move();

        }
        



        public interface IStage
        {
            MutantMovementContext Context { get; }
            void Move();

        }


        public class PassiveStage : IStage
        {
            public MutantMovementContext Context { get; }
            IPassiveMovement movement = new IdleMovement();

            public PassiveStage(MutantMovementContext context)
            {
                Context = context;
            }

            public void Move()
            {
                if (Vector3.Distance(Context.transform.position, Context.playerTransform.position) < Context.triggerLength)
                {
                    Context.isChasing = true;
                    Context.stage = new ChasingStage(Context);
                }
                else
                {
                    movement.Move(Context.rigidbody);
                }

            }

        }

        public class ChasingStage : IStage
        {
            public MutantMovementContext Context { get; }

            public ChasingStage(MutantMovementContext context)
            {
                Context = context;
            }

            public void Move()
            {
                if (Vector3.Distance(Context.transform.position, Context.playerTransform.position) > Context.chaseLength)
                {
                    Context.isChasing = false;
                    Context.stage = new PassiveStage(Context);
                    return;
                }
                Context.rigidbody.velocity = GetVelocity();
                Context.pusghDirection = Vector3.Lerp(Context.pusghDirection, Vector3.zero, Context.pushRecovery);
                if (Context.rigidbody.velocity.x > 0) Context.transform.localScale = ToLeft();
                if (Context.rigidbody.velocity.x < 0) Context.transform.localScale = ToRight();
            }


            public Vector3 GetVelocity() => (Context.playerTransform.position - Context.transform.position).normalized * Context.speed + Context.pusghDirection;
            public Vector3 ToLeft() => new Vector3(math.abs(Context.transform.localScale.x), Context.transform.localScale.y, 1);
            public Vector3 ToRight() => new Vector3(-math.abs(Context.transform.localScale.x), Context.transform.localScale.y, 1);

        }


        public class NoneStage : IStage
        {
            public MutantMovementContext Context { get; }

            public NoneStage(MutantMovementContext context)
            {
                Context = context;
            }

            public void Move()
            {
                if (Context.transform == null || Context.playerTransform == null)
                    return;
                Context.stage = new PassiveStage(Context);
            }
        }
    }


    ///
    /// observer pattern 
    ///

    public class Mag : Mutant
    {
        private List<Skeleton> skeletons = new List<Skeleton>();

        public IReadOnlyList<Skeleton> Skeletons => skeletons;
        1
        public void AddSkeleton(Skeleton skeleton)
        {
            skeletons.Add(skeleton);
        }

        public void RemoveSkeleton(Skeleton skeleton)
        {
            skeletons.Remove(skeleton);
        }

        public void BoostAll()
        {
            foreach (Skeleton skeleton in skeletons)
            {
                skeleton.OnBoost();
            }
        }

        public class Skeleton : Mutants.Skeleton
        {
            public void OnBoost()
            {
                Health.Current += 10;
                Strength.Current += 10;
            }
        }




    }






}
