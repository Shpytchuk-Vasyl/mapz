﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Part2
    {
        public static void MeasureTime(string operationName, Action action)
        {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        action.Invoke();

        stopwatch.Stop();
        Console.WriteLine($"{operationName}: {stopwatch.ElapsedMilliseconds} ms");
        }


    }

    class CompareClass
    {
        class MyClassConstructor
        {
            private int instanceVariable;

            public MyClassConstructor(int value)
            {
                instanceVariable = value;
            }
        }

        class MyClassOutsideConstructor
        {
            private int instanceVariable;

            public void InitializeVariable(int value)
            {
                instanceVariable = value;
            }
        }

        class MyClassStaticInitialization
        {
            private static int staticVariable = 42;
        }

        class MyClassStaticConstructor
        {
            private static int staticVariable;

            static MyClassStaticConstructor()
            {
                staticVariable = 42;
            }
        }


        public static void demonstrate()
        {
            int Iterations = 10_000_000;

            Part2.MeasureTime("MyClassConstructor Initialization", () =>
            {
                for (int i = 0; i < Iterations; i++)
                {
                    MyClassConstructor obj = new MyClassConstructor(i);
                }
            });

            Part2.MeasureTime("MyClassOutsideConstructor Initialization", () =>
            {
                

                for (int i = 0; i < Iterations; i++)
                {
                    MyClassOutsideConstructor obj = new MyClassOutsideConstructor();
                    obj.InitializeVariable(i);
                }
            });

            Part2.MeasureTime("MyClassStaticInitialization", () =>
            {
                for (int i = 0; i < Iterations; i++)
                {
                    MyClassStaticInitialization obj = new MyClassStaticInitialization();
                }
            });

            Part2.MeasureTime("MyClassStaticConstructor Initialization", () =>
            {
                for (int i = 0; i < Iterations; i++)
                {
                    MyClassStaticConstructor obj = new MyClassStaticConstructor();
                }
            });
        }

    }

    public struct CompareStruct
    {
        public struct MyStructConstructor
        {
            private int instanceVariable;

            public MyStructConstructor(int value)
            {
                instanceVariable = value;
            }
        }

        public struct MyStructOutsideConstructor
        {
            private int instanceVariable;

            public void InitializeVariable(int value)
            {
                instanceVariable = value;
            }
        }

        public struct MyStructStaticInitialization
        {
            private static int staticVariable;

            static MyStructStaticInitialization()
            {
                staticVariable = 42;
            }
        }

        class MyStructStaticConstructor
        {
            private static int staticVariable;

            static MyStructStaticConstructor()
            {
                staticVariable = 42;
            }
        }

        public static void demonstrate()
        {
            int Iterations = 10_000_000;

            Part2.MeasureTime("MyStructConstructor Initialization", () =>
            {
                for (int i = 0; i < Iterations; i++)
                {
                    MyStructConstructor obj = new MyStructConstructor(i);
                }
            });

            Part2.MeasureTime("MyStructOutsideConstructor Initialization", () =>
            {
                

                for (int i = 0; i < Iterations; i++)
                {
                    MyStructOutsideConstructor obj = new MyStructOutsideConstructor();
                    obj.InitializeVariable(i);
                }
            });

            Part2.MeasureTime("MyStructStaticInitialization", () =>
            {
                for (int i = 0; i < Iterations; i++)
                {
                    MyStructStaticInitialization obj = new MyStructStaticInitialization();
                }
            });

            Part2.MeasureTime("MyStructStaticConstructor Initialization", () =>
            {
                for (int i = 0; i < Iterations; i++)
                {
                    MyStructStaticConstructor obj = new MyStructStaticConstructor();
                }
            });
        }
    }

    class DemoObject : Object
    {
        private int value;

        public DemoObject(int value)
        {
            this.value = value;
        }

        public static void demonstrate()
        {
            DemoObject obj1 = new DemoObject(42);
            DemoObject obj2 = (DemoObject)obj1.MemberwiseClone();

            Console.WriteLine($"obj1.Equals(obj2): {obj1.Equals(obj2)}");
            Console.WriteLine($"obj1.GetHashCode(): {obj1.GetHashCode()}");
            Console.WriteLine($"obj1.ToString(): {obj1.ToString()}");

          
        }

        public override bool Equals(object? obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            DemoObject other = (DemoObject)obj;
            return value == other.value;
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }

        public override string? ToString()
        {
            return $"DemoObject with value: {value}";
        }

        protected new object MemberwiseClone()
        {
            return new DemoObject(this.value);
        }

    }


}
