﻿using Lab4;
using Mutants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    class Program
    {
        public static void Main()
        {
            Console.WriteLine("Mutant Game!!!");
            Mutant  s = new SkeletonGenerator().Generate();
            Console.WriteLine(s.Name);
            Console.WriteLine(s);
            Console.WriteLine(s is Dragon);
        }
    }
}
