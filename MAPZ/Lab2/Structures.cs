﻿using System;

namespace Lab2_2
{
    public interface IMyInterface
    {
        public void MyMethod();
    }



    public  struct MyStruct : IMyInterface
    {
        private int Field1 { get; set; }
        public int Field2 { get; set; }
        //protected int Field3 { get; set; }
        internal int Field4 { get; set; }
        int Field5 { get; set; }


        public void MyMethod()
        {
            Console.WriteLine("Implementation of MyMethod in MyStruct");
        }

        public void PublicMethod()
        {
            Console.WriteLine("Public Method");
        }

        private void PrivateMethod()
        {
            Console.WriteLine("Private Method");
        }
       /* protected  void ProtectedMethod()
        {
            Console.WriteLine("Protected Method");
        }*/

        internal void InternalMethod()
        {
            Console.WriteLine("Internal Method");
        }

        void DefaultMethod()
        {
            Console.WriteLine("Default Method");
        }



        public static void demonstrate()
        {
            MyStruct myObject = new MyStruct();

            myObject.MyMethod();

            myObject.PublicMethod();
            myObject.InternalMethod();
            myObject.PrivateMethod();
            myObject.DefaultMethod();
            //myObject.ProtectedMethod();

            Console.WriteLine($"Public field value: {myObject.Field2}");
            Console.WriteLine($"Internal field value: {myObject.Field4}");
            Console.WriteLine($"Default field value: {myObject.Field5}");
            // Console.WriteLine($"Protected field value: {myObject.Field3}");
            Console.WriteLine($"Private field value: {myObject.Field1}");
        }


    }



    struct DemoInnerStruct
    {

        public static void demonstrate()
        {
            InnerStruct myObject = new InnerStruct();


            myObject.PublicMethod();
            myObject.InternalMethod();
            //myObject.PrivateMethod()
            //myObject.DefaultMethod();
            //myObject.ProtectedMethod();


            Console.WriteLine($"Public field value: {myObject.Field2}");
            Console.WriteLine($"Internal field value: {myObject.Field4}");
            //Console.WriteLine($"Default field value: {myObject.Field5}");
            // Console.WriteLine($"Protected field value: {myObject.Field3}");
            // Console.WriteLine($"Private field value: {myObject.Field1}");
        }

        struct InnerStruct
        {
            private int Field1 { get; set; }
            public int Field2 { get; set; }
            //protected int Field3 { get; set; }
            internal int Field4 { get; set; }
            int Field5 { get; set; }

            public void PublicMethod()
            {
                Console.WriteLine("Public Method");
            }

            private void PrivateMethod()
            {
                Console.WriteLine("Private Method");
            }
            /*protected void ProtectedMethod()
            {
                Console.WriteLine("Protected Method");
            }*/

            internal void InternalMethod()
            {
                Console.WriteLine("Internal Method");
            }

            void DefaultMethod()
            {
                Console.WriteLine("Default Method");
            }
        }
    }

    struct DemoEnum
    {
        public enum Days : byte
        {
            None = 0b_0000_0000,
            Monday = 0b_0000_0001,  // 1
            Tuesday = 0b_0000_0010,  // 2
            Wednesday = 0b_0000_0100,  // 4
            Thursday = 0b_0000_1000,  // 8
            Friday = 0b_0001_0000,  // 16
            Saturday = 0b_0010_0000,  // 32
            Sunday = 0b_0100_0000,  // 64
            Weekend = Saturday | Sunday,
            WorkDays = Monday | Tuesday | Wednesday | Thursday | Friday,
        }


        public static void demonstrate()
        {
            Days workDays = Days.Monday | Days.Tuesday | Days.Wednesday | Days.Thursday | Days.Friday;
            Days weekend = Days.Saturday | Days.Sunday;

            Console.WriteLine("Work days: " + workDays);
            Console.WriteLine("Weekend: " + weekend);

            bool isSaturdayWorkDay = (workDays & Days.Saturday) != 0;
            Console.WriteLine("Is Saturday a work day? " + isSaturdayWorkDay);

            bool isSundayWeekend = (weekend & Days.Sunday) != 0;
            Console.WriteLine("Is Sunday a weekend day? " + isSundayWeekend);


        }

    }

    public interface IMyStructInterface
    {
        public void MyMethod();
    }

    struct DemoMultipleInheritance : IMyInterface, IMyStructInterface
    {

        void IMyInterface.MyMethod()
        {
            Console.WriteLine("Implementation of IMyInterface MyMethod in DemoMultipleInheritance");
        }
        void IMyStructInterface.MyMethod()
        {
            Console.WriteLine("Implementation of IMyStructInterface MyMethod in DemoMultipleInheritance");
        }

        public static void demonstrate()
        {
            DemoMultipleInheritance obj = new DemoMultipleInheritance();
            ((IMyInterface)obj).MyMethod();
            ((IMyStructInterface)obj).MyMethod();
        }

        
    }



    struct DemoInitialization
    {
        private static int staticField;
        private int dynamicField;


        static DemoInitialization()
        {
            staticField = 10;
            Console.WriteLine("Static constructor is called. StaticField is initialized.");
        }

        public DemoInitialization()
        {
            dynamicField = 20;
            Console.WriteLine("Instance constructor is called. DynamicField is initialized.");
        }

        public void DisplayFields()
        {
            Console.WriteLine($"StaticField: {staticField}, DynamicField: {dynamicField}");
        }

        public static void demonstrate()
        {

            Console.WriteLine($"StaticField: {DemoInitialization.staticField}");
            DemoInitialization example = new DemoInitialization
            {
                dynamicField = 30
            };

            Console.WriteLine("Field initialization using object initializer:");
            example.DisplayFields();
        }
    }


    struct DemoParameter
    {
        public static void WithoutOutRef(int a)
        {
            a = a * 2;
            Console.WriteLine($"Inside WithoutOutRef: a = {a}");
        }

        public static void WithRef(ref int b)
        {
            b = b * 2;
            Console.WriteLine($"Inside WithRef: b = {b}");
        }

        public static void WithOut(out int c)
        {
            c = 10;
            Console.WriteLine($"Inside WithOut: c = {c}");
        }

        public static void ReturnValueWithOut(out int result)
        {
            result = 42;
        }

        public static void demonstrate()
        {
            int x = 5;
            Console.WriteLine($"Before WithoutOutRef: x = {x}");
            WithoutOutRef(x);
            Console.WriteLine($"After WithoutOutRef: x = {x}");


            int y = 5;
            Console.WriteLine($"Before WithRef: y = {y}");
            WithRef(ref y);
            Console.WriteLine($"After WithRef: y = {y}");


            int z;
            WithOut(out z);
            Console.WriteLine($"After WithOut: z = {z}");


            int result;
            ReturnValueWithOut(out result);
            Console.WriteLine($"ReturnValueWithOut: result = {result}");
        }

    }

    struct DemoBoxing
    {



        public static void demonstrate()
        {
            int intValue = 42;
            object boxedValue = intValue;

            Console.WriteLine($"Boxed value: {boxedValue}");


            int unboxedValue = (int)boxedValue;
            Console.WriteLine($"Unboxed value: {unboxedValue}");


            try
            {
                double unboxedDoubleValue = (double)boxedValue;
                Console.WriteLine($"Unboxed double value: {unboxedDoubleValue}");
            }
            catch (InvalidCastException ex)
            {
                Console.WriteLine($"Error during unboxing: {ex.Message}");
            }


            Distance distance = new Distance(1000.0);

            double meters = distance;
            Console.WriteLine($"Implicit conversion to double: {meters} meters");

            int metersInt = (int)distance;
            Console.WriteLine($"Explicit conversion to int: {metersInt} meters");
        }

        struct Distance
        {
            private double value;

            public Distance(double meters)
            {
                value = meters;
            }

            public static implicit operator double(Distance distance)
            {
                return distance.value;
            }

            public static explicit operator int(Distance distance)
            {
                return (int)distance.value;
            }
        }

    }
}
