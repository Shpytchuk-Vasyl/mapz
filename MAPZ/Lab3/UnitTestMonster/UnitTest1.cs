﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mutants;
using static Mutants.Mutant;
using System.Collections.Generic;
using System.Linq;
using System;

namespace UnitTestMutant
{
    [TestClass]
    public class UnitTestMutant
    {
        List<Mutant> Mutants = new List<Mutant>
        {
            new Mutant("Goblin", new MutantAttribute(1, 100), new MutantAttribute(1, 100), new MutantAttribute(1, 100), new MutantAttribute(1, 1000)),
            new Mutant("Dragon", new MutantAttribute(1, 200), new MutantAttribute(1, 200), new MutantAttribute(1, 150), new MutantAttribute(1, 2000)),
            new Mutant("Skeleton", new MutantAttribute(1, 50), new MutantAttribute(1, 50), new MutantAttribute(1, 50), new MutantAttribute(1, 500)),
            new Mutant("Orc", new MutantAttribute(1, 150), new MutantAttribute(1, 150), new MutantAttribute(1, 100), new MutantAttribute(1, 1500)),
            new Mutant("Vampire", new MutantAttribute(1, 120), new MutantAttribute(1, 120), new MutantAttribute(1, 80), new MutantAttribute(1, 800)),
            new Mutant("Troll", new MutantAttribute(1, 180), new MutantAttribute(1, 180), new MutantAttribute(1, 120), new MutantAttribute(1, 1800)),
            new Mutant("Werewolf", new MutantAttribute(1, 130), new MutantAttribute(1, 130), new MutantAttribute(1, 90), new MutantAttribute(1, 1300)),
            new Mutant("Ghost", new MutantAttribute(1, 80), new MutantAttribute(1, 80), new MutantAttribute(1, 60), new MutantAttribute(1, 400))
        };


        [TestMethod]
        public void TestMethod1()
        {
            var powerfulMutants = Mutants.Select(mutant => mutant.Name);
            Assert.IsNotNull(powerfulMutants);
            Assert.AreEqual(Mutants.Count, powerfulMutants.Count());
        }


        [TestMethod]
        public void TestMethod2()
        {
            var dragon = Mutants
                .Where(mutant => mutant.Health.Max == Mutants[1].Health.Max)
                .Select(mutant => new {Name = mutant.Name, Strength = mutant.Strength.Current });
            Assert.IsNotNull(dragon);
            Assert.AreEqual(dragon.FirstOrDefault().Name, Mutants[1].Name);
            Assert.IsTrue(dragon.FirstOrDefault().Strength > Mutants[1].Strength.Min);
            Assert.IsTrue(dragon.FirstOrDefault().Strength < Mutants[1].Strength.Max);
        }

        [TestMethod]
        public void TestMethod3()
        {
            var ghost = Mutants.OrderBy(mutant => mutant.Health.Max);
            var dragon = Mutants.OrderBy(mutant => mutant.Health.Max).Reverse();
            var group = Mutants.GroupBy(mutant => mutant.Strength.Max > 100 ? "strong" : "weak");
            var distinct = Mutants.Distinct();
            var expect = Mutants.Except(distinct);
            
            Assert.AreEqual(dragon.FirstOrDefault().Name, Mutants[1].Name);
            Assert.AreEqual(ghost.FirstOrDefault().Name, Mutants[7].Name);
            Assert.IsTrue(group.FirstOrDefault(gr => gr.Key == "strong").Contains(Mutants[1]));
            Assert.IsTrue(Mutants.Count == distinct.Count());
            Assert.IsTrue(expect.Count() == 0);

        }

        [TestMethod]
        public void TestMethod4()
        {
            var powerfulMutants = Mutants.GetMutantsWithStrengthGreaterThan(200);
            var mutantsWithLongName = Mutants.GetMutantsWithNameLengthGreaterThan(6);

            Assert.IsTrue(powerfulMutants.Count() == 0);
            Assert.IsTrue(mutantsWithLongName.Count() == 3);
        }

        [TestMethod]
        public void TestMethod5()
        {
            var intersect = Mutants.Select(x => x.Name).Intersect(new []{ "Goblin", "Drago", "Skeleton", "Oc", "Vampire", "Troll" });
            var union = Mutants.Select(x => x.Name).Union(new[] { "Goblin", "Drago", "Skeleton", "Oc", "Vampire", "Troll" });

            Assert.AreEqual(intersect.Count(), 4);
            Assert.AreEqual(union.Count(), 10);

        }


        [TestMethod]
        public void TestMethod6()
        {
            List<Mutant> MutantsCopy = new List<Mutant>(Mutants);
            MutantsCopy.Sort(new MutantNameComparer());
            var name = MutantsCopy[0].Name;

            MutantsCopy.Sort(new Comparison<Mutant>((x, y) => ((int)(y.Speed.Max - x.Speed.Max))));
            var speed = MutantsCopy[0].Speed.Max;

            Assert.AreEqual(Mutants[1].Name, name);
            Assert.AreEqual(Mutants[1].Speed.Max, speed);

        }

        [TestMethod]
        public void TestMethod7()
        {
            Mutant[] array = Mutants.ToArray();
            Mutant[] copy = new Mutant[Mutants.Count()];
            Mutants.CopyTo(copy);

            Assert.AreEqual(Mutants.Count(), array.Length);
            Assert.AreEqual(Mutants[1], array[1]);
            Assert.AreEqual(Mutants.Count(), copy.Length);
            Assert.AreEqual(Mutants[1], copy[1]);
        }

        [TestMethod]
        public void TestMethod8()
        {
          var sorted = Mutants.OrderBy(mutant => mutant.Name).ToList();
            Assert.AreEqual(Mutants[1].Name, sorted[0].Name);
        }

        }

    [TestClass]
    public class UnitTestMission
    {
        Dictionary<int, Mission> Missions = new Dictionary<int, Mission>
    {
        { 1, new Mission(20, 200, "Kill Mutant Enemies") },
        { 2, new Mission(15, 150, "Heal Mutant Friends") },
        { 3, new Mission(25, 250, "Win Mutant Battles") },
        { 4, new Mission(10, 100, "Collect Mutant Coins") },
        { 5, new Mission(18, 180, "Lose in Mutant Fight") },
        { 6, new Mission(30, 300, "Survive for X Minutes in Mutant Game") }
    };

        [TestMethod]
        public void TestMethod1()
        {
            var missionsNames = Missions.Values.Select(mission => mission.Description);
            Assert.IsNotNull(missionsNames);
            Assert.AreEqual(Missions.Count, missionsNames.Count());
        }

        [TestMethod]
        public void TestMethod2()
        {
            var winMutantBattlesMission = Missions
                .Where(mission => mission.Value.Reward == 250)
                .Select(mission => new { Id = mission.Key, Description = mission.Value.Description });

            Assert.IsNotNull(winMutantBattlesMission);
            Assert.AreEqual(winMutantBattlesMission.FirstOrDefault()?.Id, 3);
            Assert.AreEqual(winMutantBattlesMission.FirstOrDefault()?.Description, "Win Mutant Battles");
        }

        [TestMethod]
        public void TestMethod3()
        {
            var Kill = Missions.OrderBy(mission => mission.Key);
            var Survive = Missions.OrderByDescending(mission => mission.Key); 
            var group = Missions.GroupBy(mission => mission.Key % 2 == 0 ? "even" : "odd");

            Assert.AreEqual(Kill.FirstOrDefault().Value.Description, "Kill Mutant Enemies"); 
            Assert.AreEqual(Survive.FirstOrDefault().Value.Description, "Survive for X Minutes in Mutant Game"); 
            Assert.IsTrue(group.FirstOrDefault(gr => gr.Key == "even").Contains(Missions.ElementAt(1))); 
        }


        [TestMethod]
        public void TestMethod4()
        {
            var completedMissions = Missions.CompletedMissions();
           var highRewardMissions = Missions.RewardGreaterThan(190);

            Assert.IsTrue(completedMissions.Count() == 0);
            Assert.IsTrue(highRewardMissions.Count() == 3);

        }

        [TestMethod]
        public void TestMethod5()
        {
            var intersect = Missions.Select(x => x.Key).Intersect(new[] { 1,3,5 });
            var union = Missions.Select(x => x.Key).Union(new[] { 1, 3, 5 });

            Assert.AreEqual(intersect.Count(), 3);
            Assert.AreEqual(union.Count(), 6);
        }

        [TestMethod]
        public void TestMethod6()
        {
            List<Mission > copy = new Dictionary<int, Mission>(Missions).Values.ToList();
            copy.Sort(new MissionComparer());
            var description = copy[0].Description;

            Assert.AreEqual(description, Missions[4].Description);
 
        }

        [TestMethod]
        public void TestMethod7()
        {
            var missionIdsArray = Missions.Keys.ToList();
            var missionValuesArray = Missions.Values.ToArray();

            Assert.IsNotNull(missionIdsArray);
            Assert.IsNotNull(missionValuesArray);
            Assert.AreEqual(Missions.Count, missionIdsArray.Count);
            Assert.AreEqual(Missions.Count, missionValuesArray.Length);
            Assert.AreEqual(Missions[1], missionValuesArray[0]);
            Assert.AreEqual(Missions[6].Reward, missionValuesArray[5].Reward);
        }

        [TestMethod]
        public void TestMethod8()
        {
            var sortedByCount = Missions.OrderBy(pair => pair.Key);
            var sortedByName = Missions.OrderBy(pair => pair.Value.Description);
        
            Assert.AreEqual("Collect Mutant Coins", sortedByName.First().Value.Description);
            Assert.AreEqual(1, sortedByCount.First().Key);

        }
    }




    public class MutantNameComparer : IComparer<Mutant>
   {
      public int Compare(Mutant x, Mutant y)
        {
            return string.Compare(x.Name, y.Name);
        }
    }

    public class MissionComparer : IComparer<Mission>
    {
        public int Compare(Mission x, Mission y)
        {
            return string.Compare(x.Description, y.Description);
        }
    }

    public static class MyExtensions
    {
        public static IEnumerable<Mutant> GetMutantsWithStrengthGreaterThan(this List<Mutant> mutants, int strength)
        {
            return mutants.Where(mutant => mutant.Strength.Current > strength);
        }

        public static IEnumerable<Mutant> GetMutantsWithNameLengthGreaterThan(this List<Mutant> mutants, int length)
        {
            return mutants.Where(mutant => mutant.Name.Length > length);
        }

        public static Dictionary<int, Mission> CompletedMissions(this Dictionary<int, Mission> missions)
        {
            return missions.Where(pair => pair.Value.IsCompleted()).ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        public static Dictionary<int, Mission> RewardGreaterThan(this Dictionary<int, Mission> missions, int rewardThreshold)
        {
            return missions.Where(pair => pair.Value.Reward > rewardThreshold).ToDictionary(pair => pair.Key, pair => pair.Value);
        }
    }

}
