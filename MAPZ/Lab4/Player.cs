﻿using Mutants;
using System.Text.Json;

namespace PlayerNamespace
{
    [Serializable]
    class Player : IDisposable
    {
        public Mutant Mutant { get; set; }
        public long Experience { get; set; }
        public long DNA { get; set; }
        public string PlayerName { get; set; }

        private const string SaveFileName = "player_data.dat";

        private static readonly Lazy<Player> instance = new Lazy<Player>(() => new Player());

        public static Player Instance => instance.Value;

        private Player()
        {
            LoadData();
        }

        public void Dispose()
        {
            SaveData();
        }

        private void LoadData()
        {
            try
            {
                    
                Player player = JsonSerializer.Deserialize<Player>(File.ReadAllBytes(SaveFileName));
                Mutant = player.Mutant;
                Experience = player.Experience;
                DNA = player.DNA;
                PlayerName = player.PlayerName;
            
            }
            catch (Exception ex)
            {
                Console.WriteLine("Serialization error: " + ex.Message);
                Mutant = new MutantPrototypeFactory().CreateRandomMutant();
                Experience = 0;
                DNA = 10;
                PlayerName = "player";
            }
        }

        private void SaveData()
        {
            try
            {
                File.WriteAllBytes(SaveFileName, JsonSerializer.SerializeToUtf8Bytes<Player>(this));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Serialization error: " + ex.Message);
            }
        }
    }
}
